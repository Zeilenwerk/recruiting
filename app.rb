require 'sinatra'
require 'sinatra/activerecord'
require 'require_all'

require_all 'models/*.rb'

require_relative 'db/config'

# DB-Beispieldaten
u = User.create!(name: 'Reto', birthday: '1990-01-01')
u.protocols.create!(date: '2016-04-12', text: 'Reto ist ein sehr kranker Cheib')
# Ende DB-Beispieldaten

# Startseite
get '/' do
  @users = User.all
  erb :users
end

# Impressum-Seite
get '/about' do
  erb :about
end

# Formular für neue Patienten
get '/users/new' do
  erb :new_user
end

# Speicherung neuer Patienten
post '/users/create' do
  name = params[:name]

  # Dein Code

  redirect '/'
end

# Detailansicht eines Patienten
get '/users/:id' do
  @user = User.find_by(id: params[:id])
  erb :user
end
