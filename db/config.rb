def connect
  ActiveRecord::Base.logger.level = Logger::ERROR

  Time.zone = "UTC"
  ActiveRecord::Base.default_timezone = :utc

  # Setup in-memory Database
  ActiveRecord::Base.establish_connection({:adapter => 'sqlite3', :database => 'db.sqlite'})
  ActiveRecord::Migration.verbose = false
  ActiveRecord::Migrator.up('db/migrate')
  ActiveRecord::Base.logger.level = Logger::DEBUG
end

set :lock, true
if File.exist? ('db.sqlite')
  File.delete('db.sqlite')
end
connect

before do
  connect
end
