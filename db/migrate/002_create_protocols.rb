class CreateProtocols < ActiveRecord::Migration
  def change
    create_table(:protocols) do |t|
      t.string :date, null: false
      t.string :text, null: false
      t.references :user, index: true
    end
  end
end
