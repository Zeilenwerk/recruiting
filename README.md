# Zeilenwerk Recruiting Project

## Ausgangslage

Zeilenwerk möchte eine Applikation für das Patientenmanagement in einem Spital schreiben.

Das Spital möchte seine Patienten elektronisch erfassen und jeweils pro Patient ein Protokoll führen, in dem die Befunde jedes Besuchs erfasst werden.

Das Spital hat mit der Programmierung schon begonnen. Bisher bestehen die Daten in der Datenbank, und das Spital hat eine Liste aller Patienten programmiert.

Die Seite ist bisher in [Ruby](https://www.ruby-lang.org/en/) mit dem Framework [Sinatra](http://www.sinatrarb.com/intro.html) und dem Datanbank-Adapter [ActiveRecord](http://guides.rubyonrails.org/active_record_basics.html#crud-reading-and-writing-data) programmiert. Die verlinkten Seiten geben dir weitere Informationen sowie Dokumentation und Beispielcode.

## Struktur

* Jeder Patient hat einen Namen und ein Geburtsdatum
* Jeder Patient hat 0 bis n Protokolleinträge
* Jeder Protokolleintrag hat ein Datum und einen Text

## Anforderungen

* Ergänze die Detailansicht der Patienten, indem du zusätzlich zum Namen noch das Geburtsdatum anzeigst
* Programmiere eine Detailansicht der Patienten, in denen jeweils alle zum Patienten gehörenden Protokolle aufgelistet sind.
* Programmiere eine neue URL, in der Patienten über ihren Namen gesucht werden können. Angestellte sollen eine URL wie `/names/Hanspeter` öffnen können, so dass dann der Patient namens Hanspeter angezeigt wird
* Programmiere eine Ansicht für den Nachtwärter, in der _alle_ Protokolleinträge aufgelistet. Zusätzlich soll der Name des Patienten bei jedem Eintrag angezeigt werden.
* Anonymisiere die Protokollliste von Punkt 3, in dem du nicht den vollen Namen, sondern nur den ersten Buchstaben des Namens anzeigst (Beispiel: statt "Reto" soll "R." stehen.
* Unter der Adresse `/users/new` besteht ein Formular, in der neue Patienten erfasst werden können. Ergänze den Code in `app.rb`, so dass der neue Benutzer in der Datenbank gespeichert wird.


## Installation

* Ruby 1.9+ installieren
* Im Terminal
  * Bundler installieren: `gem install bundler`
  * In Projektordner wechseln `cd recruiting`
  * Gems installieren: `bundle install`
  * App starten: `bundle exec ruby app.rb`
* Browser öffnen: `http://localhost:4567`
